from scripts.posts import BlogPost
from jinja2 import Environment, PackageLoader
import webapp2
from scripts.users import User


class Logout(webapp2.RequestHandler):

    def get(self):
        self.post()

    def post(self):
        self.response.delete_cookie("auth_token")
        redirect_url = self.request.get("redirect_url", "/")
        self.redirect(redirect_url)

app = webapp2.WSGIApplication([
    ('/authentication/logout', Logout)],
                            debug=True)
