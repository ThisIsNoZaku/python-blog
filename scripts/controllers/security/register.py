from jinja2 import Environment, PackageLoader
import webapp2
from scripts.users import User


class Register(webapp2.RequestHandler):
    def get(self):
        env = Environment(loader=PackageLoader("templates", ""))
        redirect_url = self.request.get("from", "/")
        self.response.write(
            env
            .get_template(name="register.html").render(redirect=redirect_url)
            )

    def post(self):
        error = None
        env = Environment(loader=PackageLoader("templates", ""))
        redirect_url = self.request.get("redirect_url", "/")
        username = self.request.get("username")
        password = self.request.get("password")
        confirm = self.request.get("password_confirm")
        error = None
        if not username:
            error = "Username required."
        elif not password:
            error = "Password required."
        if password != confirm:
            error = "The password and confirmation didn't match."
        if User.exists(username):
            error = "A user with this username already exists."
        if error:
            self.response.write(env.get_template("register.html")
                                .render(register_error=error,
                                        redirect=redirect_url))
            return
        token = User.register(username, password)
        self.response.set_cookie("auth_token",
                                 token.auth_token,
                                 max_age=86400)
        self.redirect(redirect_url)

app = webapp2.WSGIApplication([
    ('/authentication/register', Register)],
                            debug=True)
