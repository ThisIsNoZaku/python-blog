from scripts.posts import BlogPost
from jinja2 import Environment, PackageLoader
import webapp2
from scripts.users import User


class Login(webapp2.RequestHandler):
    def get(self):
        env = Environment(loader=PackageLoader("templates", ""))
        redirect_url = self.request.get("from", "/")
        self.response.write(env.get_template(name="login.html")
                            .render(redirect=redirect_url))

    def post(self):
        error = None
        env = Environment(loader=PackageLoader("templates", ""))
        redirect_url = self.request.get("redirect_url", "/")
        error = None
        token = None
        if not User.exists(self.request.get("username")):
            error = "No account with that name exists."
        else:
            token = User.authenticate(self.request.get("username"),
                                      self.request.get("password"))
            if not token:
                error = "The username and password combination was incorrect."
        if error:
            self.response.write(env.get_template("login.html").render(
                                login_error=error,
                                redirect=redirect_url))
            return
        self.response.set_cookie("auth_token", token, max_age=86400)
        self.redirect(redirect_url)

app = webapp2.WSGIApplication([
    ('/authentication/login', Login)],
                            debug=True)
