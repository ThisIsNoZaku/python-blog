from scripts.posts import BlogPost
from scripts.users import User
from jinja2 import Environment, PackageLoader
import webapp2
import datetime
import sys


class Blog(webapp2.RequestHandler):
    def get(self):
        env = Environment(loader=PackageLoader("templates", ""))
        user_auth_token = self.request.cookies.get("auth_token")
        user = User.get_by_token(user_auth_token)
        if self.request.get("post_id"):
            post = BlogPost.get_by_id(int(self.request.get("post_id")))
            out = (env.get_template(name="post.html")
                   .render(post=post,
                           user=user,
                           redirect="/blog/post?post_id=" + str(post.key.id()))
                   )
        else:
            posts = BlogPost.all()
            out = env.get_template(name="index.html").render(
                                 posts=BlogPost.all(),
                                 user=user,
                                 redirect="/")

        self.response.write(out)
        self.response.headers['Content-Type'] = "text/html"

app = webapp2.WSGIApplication([
    ('/', Blog),
    ('/blog', Blog),
    ('/blog/post', Blog)],
                              debug=True)
