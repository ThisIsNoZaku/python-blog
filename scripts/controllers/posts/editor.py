from scripts.posts import BlogPost
from scripts.users import User
from jinja2 import Environment, PackageLoader
import webapp2
import datetime
import sys

"""
Handler for editing
"""
class Editor(webapp2.RequestHandler):
    """
    Displays the post editing page, populated with the post with
    the given id if one is specified and a corresponding post
    exists.
    """
    def get(self):
        post_id = self.request.get("post_id", None)
        env = Environment(loader=PackageLoader("templates", ""))
        user_auth_token = self.request.cookies.get("auth_token")
        user = User.get_by_token(user_auth_token)
        if post_id:
            post = BlogPost.get_by_id(int(post_id))
        else:
            post = BlogPost(title="", content="")
        sys.stdout.flush()
        template = env.get_template("edit.html")
        out = template.render(post=post,
                              user=user)
        self.response.write(out)
    """
    Persist the submitted post. If an id is given, the entity with
    the same id is updated with the submitted post. Otherwise, a
    new entity is created.
    """
    def post(self):
        post_id = self.request.get("post_id", None)
        user_auth_token = self.request.cookies.get("auth_token")
        user = User.get_by_token(user_auth_token)
        post = None
        if post_id:
            post = BlogPost.get_by_id(int(post_id))
        if post and post.poster != user:
            self.redirect("/authentication/login")
            return
        if post:
            post.title = self.request.get("content")
            post.content = self.request.get("content")
            post.post_date = datetime.datetime.now().date()
        else:
            post = BlogPost(title=self.request.get("title"),
                            content=self.request.get("content"),
                            post_date=datetime.datetime.now().date(),
                            poster=user,
                            likes=[])
        key = post.put()
        self.redirect("/blog/post?post_id=%s" % key.id())


app = webapp2.WSGIApplication([
    ('/blog/post/edit', Editor),
    ('/blog/post/create', Editor)],
                            debug=True)
