from scripts.posts import BlogPost
from scripts.users import User
import webapp2


class Likes(webapp2.RequestHandler):
    """
    Adds a like from the current user for a specified post, if
    the liking user and post creator are different and the user
    has not already liked the post.
    """
    def post(self):
        user_auth_token = self.request.cookies.get("auth_token")
        user = User.get_by_token(user_auth_token)
        post = BlogPost.get_by_key(self.request.get("post_key"))
        if ((user) and
                (post.poster.nickname != user.nickname) and
                (user.nickname not in post.likes)):
            post.likes.append(user.nickname)
            post.put()
        self.redirect(self.request.headers["referer"])


app = webapp2.WSGIApplication([('/blog/post/like', Likes)],
                              debug=True)
