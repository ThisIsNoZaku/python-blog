from scripts.posts import BlogPost
import webapp2
import sys
import imp


class Delete(webapp2.RequestHandler):
    def get(self):
        BlogPost.get_by_id(int(self.request.get("post_id"))).key.delete()
        self.redirect("/")

app = webapp2.WSGIApplication([('/blog/post/delete', Delete)],
                              debug=True)
