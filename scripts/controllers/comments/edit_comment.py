from scripts.users import User
from scripts.comments import Comment
from scripts.posts import BlogPost
from google.appengine.ext import ndb
import webapp2
import datetime


class EditComments(webapp2.RequestHandler):
    def post(self):
        comment_poster = User.get_by_token(self.request.cookies["auth_token"])
        if not comment_poster:
            self.redirect("/authentication/login")
            return
        post_id = self.request.get("post_key")
        post = BlogPost.get_by_id(int(post_id))
        comment_id = self.request.get("comment_id")
        existing_comment = next((c for c in post.comments
                                 if c.id == comment_id), None)
        existing_comment.content = self.request.get("comment_content")
        existing_comment.comment_datetime = datetime.datetime.now()
        comment = existing_comment
        post.put()
        self.redirect(self.request.headers["Referer"])

app = webapp2.WSGIApplication([
    ('/blog/post/comment/edit', EditComments)],
                            debug=True)
