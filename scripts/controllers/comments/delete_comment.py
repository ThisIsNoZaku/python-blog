from scripts.users import User
from scripts.comments import Comment
from scripts.posts import BlogPost
from google.appengine.ext import ndb
import webapp2
import datetime
import sys


class DeleteComments(webapp2.RequestHandler):
    def post(self):
        user = User.get_by_token(self.request.cookies["auth_token"])
        post_id = self.request.get("post_key")
        comment_id = self.request.get("comment_id")
        post = BlogPost.get_by_id(int(post_id))
        to_delete = next((c for c in post.comments if c.id == comment_id),
                         None)
        if not user or to_delete.commenter != user.nickname:
            self.redirect("/authentication/login")
            return
        post.comments = [c for c in post.comments if c != to_delete]
        post.put()
        self.redirect(self.request.headers["referer"])

app = webapp2.WSGIApplication([
    ('/blog/post/comment/delete', DeleteComments)],
                            debug=True)
