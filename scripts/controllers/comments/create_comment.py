from scripts.users import User
from scripts.comments import Comment
from scripts.posts import BlogPost
from google.appengine.ext import ndb
import webapp2
import datetime
import uuid
import sys


class CreateComments(webapp2.RequestHandler):
    def post(self):
        comment_poster = User.get_by_token(self.request.cookies["auth_token"])
        if not comment_poster:
            self.redirect("/authentication/login")
            return
        post_id = self.request.get("post_key")
        post = BlogPost.get_by_id(int(post_id))
        comment = Comment(commenter=comment_poster.nickname,
                          comment_datetime=datetime.datetime.now(),
                          content=self.request.get("comment_content"),
                          id=uuid.uuid4().hex)
        print("comment id: " + comment.id)
        sys.stdout.flush()
        post.comments.append(comment)
        post.put()
        self.redirect(self.request.headers["Referer"])

app = webapp2.WSGIApplication([
    ('/blog/post/comment', CreateComments)],
                            debug=True)
