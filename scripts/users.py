from google.appengine.ext import ndb
import webapp2
import datetime
import sys
import hashlib
import random


class User(ndb.Model):
    nickname = ndb.StringProperty(required=True)
    password = ndb.StringProperty(required=True)
    pass_salt = ndb.StringProperty(required=True)

    @classmethod
    def get_by_token(self, authentication_token):
        """Returns the User instance associated with the given authentication
        token."""
        result = (UserToken.query(UserToken.auth_token == authentication_token)
                  .fetch(1))
        if result:
            user = ndb.Key(User, int(result[0].user_id)).get()
            return user

    @classmethod
    def get_token_by_key(self, user):
        """Return the authentication token container for the given User
        instance."""
        result = (UserToken.query(UserToken.user_id == str(user.key.id()))
                  .fetch(1))
        return result[0] if result else None

    @classmethod
    def authenticate(self, username, password):
        """Attempt to authenticate with the given username and password.
        Returns the generated authentication token if successful, otherwise
        None.
        """
        user = User.query(User.nickname == username).fetch(1)
        hashed_pass = hashlib.sha256(user[0].pass_salt + password).hexdigest()
        if not user or user[0].password != hashed_pass:
            return None
        token_container = (UserToken.query(User.nickname == username)
                           .fetch(1))
        auth_token = (hashlib.sha256(user[0].pass_salt + user[0].nickname)
                      .hexdigest())
        if not token_container:
            token_container = UserToken(user_id=str(user[0].key.id()),
                                        auth_token=auth_token)
        else:
            token_container.auth_token = auth_token
        token_container.put()
        return auth_token

    @classmethod
    def register(self, username, password):
        """Creates a new user with the given username and password
        and returns a container with the new User instance and
        generated authentication token.
        """
        if not User.exists(username):
            salt = str(random.getrandbits(32)).encode(encoding="UTF-8")
            user = User(nickname=username, pass_salt=salt,
                        password=hashlib.sha256(salt + password).hexdigest())
            key = user.put()
            auth_token = (hashlib.sha256(user.pass_salt + user.nickname)
                          .hexdigest())
            token_container = UserToken(user_id=str(user.key.id()),
                                        auth_token=auth_token)
            token_container.put()
            return token_container

    @classmethod
    def exists(self, username):
        """Returns whether or not a user with the given username exists."""
        user = User.query(User.nickname == username).fetch(1)
        return True if user else False


class UserToken(ndb.Model):
    user_id = ndb.StringProperty(required=True)
    auth_token = ndb.StringProperty(required=True)
