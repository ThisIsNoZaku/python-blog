from google.appengine.ext import ndb
from scripts.users import User
from scripts.comments import Comment


class BlogPost(ndb.Model):
    title = ndb.StringProperty(required=True)
    content = ndb.StringProperty(required=True)
    post_date = ndb.DateProperty()
    poster = ndb.StructuredProperty(User, required=True)
    comments = ndb.StructuredProperty(Comment, repeated=True)
    likes = ndb.PickleProperty(required=True)

    @classmethod
    def all(self):
        """Utility method to easily get all BlogPosts."""
        return self.query().fetch()
