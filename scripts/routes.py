import webapp2
from scripts.controllers.posts.blog import Blog
from scripts.controllers.posts.editor import Editor
from scripts.controllers.posts.likes import Likes
from scripts.controllers.posts.delete_post import Delete
from scripts.controllers.comments.create_comment import CreateComments
from scripts.controllers.comments.delete_comment import DeleteComments
from scripts.controllers.comments.edit_comment import EditComments
from scripts.controllers.security.login import Login
from scripts.controllers.security.logout import Logout
from scripts.controllers.security.register import Register


app = webapp2.WSGIApplication([
    ('/', Blog),
    ('/blog', Blog),
    ('/blog/post', Blog),
    ('/blog/post/edit', Editor),
    ('/blog/post/create', Editor),
    ('/blog/post/like', Likes),
    ('/blog/post/delete', Delete),
    ('/blog/post/comment', CreateComments),
    ('/blog/post/comment/edit', EditComments),
    ('/blog/post/comment/delete', DeleteComments),
    ('/blog/post/comment', CreateComments),
    ('/authentication/login', Login),
    ('/authentication/logout', Logout),
    ('/authentication/register', Register)],
                              debug=True)
