from google.appengine.ext import ndb
import datetime


class Comment(ndb.Model):
    commenter = ndb.StringProperty(required=True)
    comment_datetime = ndb.DateTimeProperty(required=True)
    content = ndb.StringProperty(required=True)
    id = ndb.StringProperty(required=True)
