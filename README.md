# Blog
This application is a simple blog program, accessible at https://multiuser-blog-141410.appspot.com/

Supports multiple users who may make, edit and delete posts; add, edit and delete comment on posts; and "like" posts by other users.

### Installation
A version of the project can be deployed locally. It requires the Google App Engine SDK.

After extraction, start the GAE development server. This can be done via command line using "dev_appserver.py app.yaml"

By default, the local instance will be accessible at localhost:8080 via a browser.